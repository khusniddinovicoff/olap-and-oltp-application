-- Retrieve the total sales amount for each product category for a specific time period --
SELECT
    p.prod_category,
    SUM(s.amount_sold) AS total_sales_amount
FROM
    sh.sales s
JOIN
    sh.products p ON s.prod_id = p.prod_id
JOIN
    sh.times t ON s.time_id = t.time_id
WHERE
    t.calendar_month_desc = '2000-04' -- You can change month period here
	-- t.calendar_year = '2000' -- If you want to search by year period you can uncommit this code and put the other one to commit
GROUP BY
    p.prod_category;

--Calculate the average sales quantity by region for a particular product--
SELECT
    c.country_region,
    ROUND(AVG(s.quantity_sold), 2) AS avg_sales_quantity
FROM
    sh.sales s
JOIN
    sh.customers cust ON s.cust_id = cust.cust_id
JOIN
    sh.countries c ON cust.country_id = c.country_id
WHERE
    s.prod_id = 13 -- You can change product id here
GROUP BY
    c.country_region;
	
-- Find the top five customers with the highest total sales amount
SELECT
    cust.cust_id,
    CONCAT(cust.cust_first_name, ' ', cust.cust_last_name) AS customer_name,
    SUM(s.amount_sold) AS total_sales_amount
FROM
    sh.sales s
JOIN
    sh.customers cust ON s.cust_id = cust.cust_id
GROUP BY
    cust.cust_id, customer_name
ORDER BY
    total_sales_amount DESC
LIMIT 5;